import './App.css';
import Header from  './components/Header';
//import HeaderSecond from  './components/HeaderSecond';
//import Container from './components/Container';
import Home from './components/Home';
function App() {
  
  return (
    <div className="App">
      <Header/>
      <Home/>
    </div>
  );
}

export default App;
