import axios from 'axios';
import {API_BASE_ENDPOINT} from './constants';

export const axiosCall =(method, url, payLoad)=>{
    return axios({
        method,
        url:`${API_BASE_ENDPOINT}${url}`,
        data:payLoad,
        withCredentials: false,
        headers: {
          'Access-Control-Allow-Origin' : '*',
          'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS',   
      }
      }).then(function (response) {
        return response
        }).catch(function (error) {
        return error 
      })
}