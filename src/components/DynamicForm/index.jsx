import React, { useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { ucFirst } from "./../../utils/helper";
import {axiosCall} from './../../Services/helper'; 
import {updateWith, constant} from 'lodash';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    padding: "5px 10px",
  },
  formRow: {
    display: "flex",
  },
  formLabel: {
    flex: 1,
    textAlign: "left",
    color: "#000",
    fontWeight: "600",
    padding: "2% 0%",
  },
  formInput: {
    flex: 3,
  },
  input: {
    width: "80%",
    height: "32px",
    border: "none",
    borderBottom: "1px solid #c7c7c7",
    background: "transparent",
    padding: "8px",
    margin: "4px",
  },
  select: {
    width: "80%",
    height: "48px",
    border: "none",
    borderBottom: "1px solid #c7c7c7",
    background: "transparent",
    padding: "8px",
    margin: "4px",
  },
}));

export default function DynamicForm(props) {
  const classes = useStyles();
  const [form, updateForm] = React.useState(props.formObj.form);
  useEffect(() => {
    updateForm(props.formObj.form);
  }, [props]);

  const updateFormObj=(field, value)=>{
      let current_field = form.findIndex(eachField=>eachField.field== field )
      const updatedForm = updateWith(form, `${current_field}.value`, constant(value), Object)
      updateForm(updatedForm);
  }
  const getHtmlNode = (eachHmlNode) => {
    let formNode = "";
    switch (eachHmlNode.type) {
      case "input":
        formNode = (
          <div className={classes.formInput}>
            <div>
              <input className={classes.input} id={eachHmlNode.field} name={eachHmlNode.field} onBlur={(e)=>{updateFormObj(eachHmlNode.field, e.target.value)}}/>
            </div>
            <div>{}</div>
          </div>
        );
        break;
      case "select":
        formNode = (
          <div className={classes.formInput}>
            <div>
              <select className={classes.select} id={eachHmlNode.field} name={eachHmlNode.field} onChange={(e)=>{updateFormObj(eachHmlNode.field, e.target.value)}}>
                  <option value={null}>Choose Value</option>
                {eachHmlNode.options &&
                  eachHmlNode.options.map((eachNode, index) => {
                    return (
                      <option
                        key={`${eachHmlNode.field}_${eachNode}_select_option`}
                      >
                        {ucFirst(eachNode)}
                      </option>
                    );
                  })}
              </select>
            </div>
            <div>{}</div>
          </div>
        );
        break;
      case "textArea":
        formNode = (
          <div className={classes.formInput}>
            <div>
              <textarea className={classes.select} id={eachHmlNode.field} name={eachHmlNode.field} onChange={(e)=>{updateFormObj(eachHmlNode.field, e.target.value)}}></textarea>
            </div>
            <div>{}</div>
          </div>
        );
        break;
      case "radio":
        formNode = (
          <div className={classes.formInput}>
            <div>
              <radio className={classes.select} id={eachHmlNode.field} name={eachHmlNode.field} onChange={(e)=>{updateFormObj(eachHmlNode.field, e.target.value)}}/>
            </div>
            <div>{}</div>
          </div>
        );
        break;
      case "checkbox":
        formNode = (
          <div className={classes.formInput}>
            <div>
              <checkbox className={classes.select} id={eachHmlNode.field} name={eachHmlNode.field} onChange={(e)=>{updateFormObj(eachHmlNode.field, e.target.value)}}/>
            </div>
            <div>{}</div>
          </div>
        );
        break;
      default:
        formNode = <></>;
    }
    return (
      <><div key={eachHmlNode.field} className={classes.formRow}>
        <div key={`${eachHmlNode.field}_form`} className={classes.formLabel}>
          <label key={`${eachHmlNode.field}_form_label`}>
            {eachHmlNode.title}
          </label>
        </div>
        {formNode}
      </div>
      </>
    );
  };
  const submitFrom= async()=>{
    const response = await axiosCall(props.formObj.method, props.formObj.actionEndpoint,null);
    props.onFormSubmit(response, form)
  }
  return (
    <div className={classes.root}>
      {form &&
        form.map((eachNode, index) => {
          return getHtmlNode(eachNode);
        })
        }
        {form && <Button variant="contained" color="primary" onClick={()=>{submitFrom(form)}}>Submit</Button>}
    </div>
  );
}
