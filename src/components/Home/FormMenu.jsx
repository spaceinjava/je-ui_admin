import * as React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { get } from "lodash";
import { ucFirst } from "./../../utils/helper";

const useStyles = makeStyles({
  secondHeaderWrapper: {
    backgroundColor: "#4c4a4a",
    borderTop: "3px solid #61a4cc",
    borderBottom: "1px solid #9a9696",
  },
  navbarDisplayFlex: {
    display: `flex`,
    maxWidth: "inherit",
    background: "#b1b0b252",
    padding: "10px",
    fontSize: "16px",
    fontWeight: "600",
    color: "#4c4a4a",
    border: "1px dotted",
  },
  menuTitle: {
    flex: 1,
    cursor: "pointer",
    "&:hover, &:focus": {
      color: "#000",
      fontWeight: "bold",
      textDecoration: "underline",
    },
  },
  navDisplayFlex: {
    display: `flex`,
    justifyContent: `space-between`,
  },
  linkText: {
    textDecoration: `none`,
    textTransform: `uppercase`,
    color: `white`,
  },
});

const Header = (props) => {
  const classes = useStyles();
  const actions = get(props.selectedModule, "actions", []);
  const [selectedMenu, setSelectedMenu]= React.useState();
  
  return actions.length ? (
    <div className={classes.navbarDisplayFlex}>
      {actions.map((eachAction, index) => (
        <div
          className={classes.menuTitle}
          onClick={() => {
            props.selectedForm(eachAction);
            setSelectedMenu(eachAction)
          }}
          key={`submenu_${index}`}
        >
          {eachAction && ucFirst(eachAction.action)}
        </div>
      ))}
    </div>
  ) : (
    <></>
  );
};
export default Header;
