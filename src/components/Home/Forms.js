export const forms=[
    {menuTitle:'Admin', 
    actions:[
        {action:'create',
        form:[
            {title:'First Name',field:'firstName', type:'input', isRequired:true, value:null},
            {title:'Last Name',field:'lastName', type:'input', isRequired:true, value:null},
            {title:'Gender',field:'gender', type:'select', isRequired:true,value:null, options:['male', 'female', 'N/A']}
        ], actionEndpoint:'/todos/1',method:'GET'
        },
        {action:'update',
         form:[   {title:'First Name',field:'firstName', type:'input', isRequired:true},
            {title:'Last Name',field:'lastName', type:'input', isRequired:true},
            {title:'Gender',field:'gender', type:'select', isRequired:true, options:['male', 'female', 'N/A']}
        ], actionEndpoint:'/todos/1',method:'PUT'
        }
    ],
    serviceEnpPoint:'/admin/'},
    {menuTitle:'Companies', 
    actions:[
        {action:'create',
        form:[
            {title:'Company Name',field:'companyName', type:'input', isRequired:true},
            {title:'Location',field:'Location', type:'input', isRequired:true},
            {title:'Address',field:'Address', type:'textArea', isRequired:true}
        ], actionEndpoint:'/todos/1'}
    ],
    serviceEnpPoint:'/companies/'}
]