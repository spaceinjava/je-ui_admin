import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import HeaderSecond from "./../HeaderSecond";
import Container from "./../Container";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
    margin: "8px",
    border: "1px #777 groove",
    background: "#fcf6f6",
    height: "1000px",
  },
  cardContainer: {
    borderRadius: "10px",
  },
  containerWrapper: {
    marginTop: "8px",
    backgroundColor: "#f6f6f6",
  },
}));

export default function CenteredGrid() {
  const classes = useStyles();

  const onSecondMenuClick = (selectedForm) => {
    setSelectedOption(selectedForm);
  };
  const [selectedOption, setSelectedOption] = React.useState({});
  return (
    <div className={classes.root}>
      <HeaderSecond onSecondMenuClick={onSecondMenuClick} />
      <Container selectedOption={selectedOption} />
    </div>
  );
}
