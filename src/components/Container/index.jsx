import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import FormMenu from "./../Home/FormMenu";
import DynamicForm from "./../DynamicForm";
import { ucFirst } from "./../../utils/helper";
import ReactJson from "react-json-view";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
    margin: "8px",
    border: "1px #777 solid",
    background: "#fcf6f6",
    height: "1000px",
  },
  cardContainer: {
    borderRadius: "10px",
  },
  subTitle: {
    backgroundColor: "#b1b0b252",
    padding: "10px",
    color: "#4c4a4a",
  },
  jsonView: {
    textAlign: "left",
  },
  containerWrapper: {
    marginTop: "8px",
    backgroundColor: "#f6f6f6",
  },
  breadCrumbs: {
    textAlign: "left",
    padding: "8px 0px",
    fontSize: "14px",
  },
}));

export default function CenteredGrid(props) {
  const classes = useStyles();
  const [formObj, setSelectedForm] = React.useState([]);
  const [formResponse, onFormResponse] = React.useState(null);
  const [formPayLoad, setPayLoad] = React.useState(null);
  
  return (
    <div className={classes.root}>
      <Grid container className={classes.containerWrapper}>
        <Grid item xs={8}>
          <Paper className={classes.paper}>
            <div className={classes.cardContainer}>
              {props.selectedOption.menuTitle}
              &nbsp;Form
              <FormMenu
                selectedModule={props.selectedOption}
                selectedForm={(formObj) => setSelectedForm(formObj)}
              />
              <div className={classes.breadCrumbs}>
                {props.selectedOption.menuTitle}/{ucFirst(formObj.action)}
              </div>
              <DynamicForm
                formObj={formObj}
                onFormSubmit={(formResponsePayload, formData) => {
                  onFormResponse(formResponsePayload);
                  setPayLoad(
                    formData.map((eachField) => {
                      return { [eachField.field]: eachField.value };
                    })
                  );
                }}
              />
            </div>
          </Paper>
        </Grid>
        <Grid item xs={4}>
          <Paper className={classes.paper}>
            <div className={classes.subTitle}>Payload/Response</div>
            {formPayLoad && (
              <div className={classes.jsonView}>
                <h5>Payload</h5>
                <ReactJson src={formPayLoad} theme="monokai" />
              </div>
            )}
            {formResponse && (
              <div className={classes.jsonView}>
                <h5>Response</h5>
                <ReactJson src={formResponse} theme="monokai" />
              </div>
            )}
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
}
