import * as React from "react";
import {
  AppBar,
  Toolbar,
  List,
  ListItem,
  ListItemText,
  Container,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { forms } from "./Home/Forms";

const useStyles = makeStyles({
  secondHeaderWrapper: {
    backgroundColor: "#4c4a4a",
    borderTop: "3px solid #61a4cc",
    borderBottom: "1px solid #9a9696",
  },
  navbarDisplayFlex: {
    display: `flex`,
    justifyContent: `space-between`,
    maxWidth: "inherit",
  },
  navDisplayFlex: {
    display: `flex`,
    justifyContent: `space-between`,
  },
  linkText: {
    textDecoration: `none`,
    textTransform: `uppercase`,
    color: `white`,
  },
  active: {
    fontWeight: "bold",
    backgroundColor: "red",
    padding: 4,
  },
});

const navLinks = forms;

const Header = (props) => {
  const classes = useStyles();
  const [selectedMenu, setSelectedMenu] = React.useState(null);

  return (
    <AppBar position="static" className={classes.secondHeaderWrapper}>
      <Toolbar>
        <Container maxWidth="md" className={classes.navbarDisplayFlex}>
          <List
            component="nav"
            aria-labelledby="main navigation"
            className={classes.navDisplayFlex}
          >
            {navLinks.map((eachMenu, index) => (
              <ListItem
                key={eachMenu.menuTitle}
                button
                onClick={() => {
                  props.onSecondMenuClick(eachMenu);
                  setSelectedMenu(eachMenu.menuTitle);
                }}
                className={
                  selectedMenu === eachMenu.menuTitle
                    ? classes.active
                    : classes.inActive
                }
              >
                <ListItemText
                  key={eachMenu.index}
                  primary={eachMenu.menuTitle}
                />
              </ListItem>
            ))}
          </List>
        </Container>
      </Toolbar>
    </AppBar>
  );
};
export default Header;
